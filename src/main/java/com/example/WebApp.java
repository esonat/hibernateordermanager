package com.example;

import com.example.config.BeanConfiguration;
import com.example.config.WebConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication(exclude = {org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration.class})
@Import({WebConfiguration.class,BeanConfiguration.class})
public class WebApp {

    public static void main(String[] args){
        SpringApplication.run(WebApp.class,args);
    }
}
