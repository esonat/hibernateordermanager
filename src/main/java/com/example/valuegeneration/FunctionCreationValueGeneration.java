package com.example.valuegeneration;

import org.hibernate.tuple.AnnotationValueGeneration;
import org.hibernate.tuple.GenerationTiming;
import org.hibernate.tuple.ValueGenerator;

import java.util.Date;

public class FunctionCreationValueGeneration implements
        AnnotationValueGeneration<FunctionCreationTimestamp>{

    @Override
    public void initialize(FunctionCreationTimestamp annotation,Class<?> propertyType){

    }

    public GenerationTiming getGenerationTiming(){
        return GenerationTiming.INSERT;
    }

    public ValueGenerator<?> getValueGenerator(){
        return (session, owner)->new Date();
    }

    public boolean referenceColumnInSql() {
        return false;
    }

    public String getDatabaseGeneratedReferencedColumnValue() {
        return null;
    }
}
