package com.example.repository;

import com.example.entities.Country;
import com.example.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("countryRepository")
public class CountryRepository implements GenericRepository<Country,Integer>{

    @Override
    public Country findById(Integer id) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        Country country=session.get(Country.class,id);

        session.close();
        sessionFactory.close();

        return country;
    }

    @Override
    public void add(Country entity) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        session.save(entity);

        tx.commit();
        session.close();
        sessionFactory.close();
    }

    @Override
    public void update(Integer id,Country entity) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        Country country=session.get(Country.class,id);
        country.setName(entity.getName());

        session.save(country);

        tx.commit();
        session.close();
        sessionFactory.close();
    }

    @Override
    public void delete(Integer id) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        Country country=session.get(Country.class,id);
        session.delete(country);

        tx.commit();
        session.close();
        sessionFactory.close();;
    }

    @Override
    public List<Country> findAll() {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        List<Country> countrys=(List<Country>)session.createCriteria(Country.class).list();

        session.close();
        sessionFactory.close();

        return countrys;
    }

    public Country findByName(String name) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        Query query=session.createQuery("FROM Country B WHERE B.name= :name");
        query.setParameter("name",name);
        List<Country> list=query.list();

        if(list==null || list.size()==0) return null;

        return list.get(0);
    }
}
