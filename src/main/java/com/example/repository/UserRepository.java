package com.example.repository;

import com.example.entities.*;
import com.example.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userRepository")
public class UserRepository implements GenericRepository<User,Integer>{
    private Logger logger= LoggerFactory.getLogger(UserRepository.class);

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public User findById(Integer id) throws Exception{
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        User brand=session.get(User.class,id);

        session.close();
        sessionFactory.close();

        return brand;
    }

    @Override
    public void add(User entity)throws Exception {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        session.save(entity);

        tx.commit();
        session.close();
        sessionFactory.close();
    }

    public void add(User user, List<UserPhone> phones, List<Role> roles) throws Exception
    {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        try {
            Transaction tx=session.beginTransaction();

            session.save(user);

            tx.commit();

        }catch (Exception e){
            throw new Exception("Exception while saving user");
        }

        for (UserPhone phone : phones) {
            try {
                Transaction tx=session.beginTransaction();

                phone.setUser(user);
                //user.getUserInfo().getPhones().add(phone);
                session.save(phone);

                tx.commit();

            }catch (Exception e){
                throw new Exception("Exception while adding phones to user");
            }
        }

        for (Role role : roles) {
                try{

                    Transaction tx=session.beginTransaction();

                    Query query=session.createQuery("FROM Role R WHERE R.name= :name");
                    query.setParameter("name",role.getName());
                    List<Role> list=query.list();

                    Role addedRole=list.get(0);

//                    role=roleRepository.findByName(role.getName());
//                    logger.info("Role="+role.getName());

                    UserRole userRole = new UserRole();
                    userRole.setRole(addedRole);
                    userRole.setUser(user);

                    user.getUserRoles().add(userRole);
                    addedRole.getUserRoles().add(userRole);

                    session.saveOrUpdate(userRole);
                    logger.info("Role updated");

                    tx.commit();
                }catch (Exception e){
                    throw new Exception("Exception while adding roles to user");
                }
        }


        session.close();
        sessionFactory.close();
    }

    @Override
    public void update(Integer id,User entity) throws Exception{
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        User user=session.get(User.class,id);

        user.setUserInfo(entity.getUserInfo());
        user.setUserLoginInfo(entity.getUserLoginInfo());
        user.setUserRoles(entity.getUserRoles());
        user.setVersion(entity.getVersion());

        session.save(user);

        tx.commit();
        session.close();
        sessionFactory.close();
    }

    @Override
    public void delete(Integer id) throws Exception{
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        User user=session.get(User.class,id);
        session.delete(user);

        tx.commit();
        session.close();
        sessionFactory.close();;
    }

    @Override
    public List<User> findAll() throws Exception{
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        Query query=session.createQuery("FROM User U");
        List<User> users=query.list();

        session.close();
        sessionFactory.close();

        return users;
    }

    public User findByName(String name)throws Exception{
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        Query query=session.createQuery("FROM User U WHERE U.userInfo.name= :name");
        query.setParameter("name",name);

        List<User> list=query.list();

        if(list==null || list.size()==0) return null;


        session.close();
        sessionFactory.close();

        return list.get(0);
    }
}
