package com.example.repository;

import com.example.entities.Role;
import com.example.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("roleRepository")
public class RoleRepository implements GenericRepository<Role,Integer> {

    @Override
    public Role findById(Integer id) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        Role role=session.get(Role.class,id);

        session.close();
        sessionFactory.close();

        return role;
    }

    @Override
    public void add(Role entity) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        session.save(entity);

        tx.commit();
        session.close();
        sessionFactory.close();
    }

    @Override
    public void update(Integer id,Role entity) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        Role role=session.get(Role.class,id);
        role.setName(entity.getName());

        session.save(role);

        tx.commit();
        session.close();
        sessionFactory.close();
    }

    @Override
    public void delete(Integer id) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        Role role=session.get(Role.class,id);
        session.delete(role);

        tx.commit();
        session.close();
        sessionFactory.close();;
    }

    @Override
    public List<Role> findAll() {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        List<Role> roles=(List<Role>)session.createCriteria(Role.class).list();

        session.close();
        sessionFactory.close();

        return roles;
    }

    public Role findByName(String name){
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        Query query=session.createQuery("FROM Role R WHERE R.name= :name");
        query.setParameter("name",name);
        List<Role> list=query.list();

        if(list==null || list.size()==0) return null;

        return list.get(0);
    }
}
