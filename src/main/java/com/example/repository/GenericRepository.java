package com.example.repository;

import java.util.List;

public interface GenericRepository<T,E>{
    T findById(E id) throws Exception;

    void add(T entity) throws Exception;

    void update(E id,T entity) throws Exception;

    void delete(E id) throws Exception;

    List<T> findAll() throws Exception;
}
