package com.example.repository;

import com.example.entities.City;
import com.example.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository("cityRepository")
public class CityRepository implements GenericRepository<City,Integer> {
    
    @Override
    public City findById(Integer id) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        City city=session.get(City.class,id);

        session.close();
        sessionFactory.close();

        return city;
    }

    @Override
    public void add(City entity) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        session.save(entity);

        tx.commit();
        session.close();
        sessionFactory.close();
    }

    @Override
    public void update(Integer id,City entity) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        City city=session.get(City.class,id);
        city.setName(entity.getName());

        session.save(city);

        tx.commit();
        session.close();
        sessionFactory.close();
    }

    @Override
    public void delete(Integer id) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        City city=session.get(City.class,id);
        session.delete(city);

        tx.commit();
        session.close();
        sessionFactory.close();;
    }

    @Override
    public List<City> findAll() {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        List<City> cities=(List<City>)session.createCriteria(City.class).list();

        session.close();
        sessionFactory.close();

        return cities;
    }

    public City findByName(String name) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        Query query=session.createQuery("FROM City C WHERE C.name= :name");
        query.setParameter("name",name);
        List<City> list=query.list();

        if(list==null || list.size()==0) return null;

        return list.get(0);
    }
}
