package com.example.repository;

import com.example.entities.Brand;
import com.example.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("brandRepository")
public class BrandRepository implements GenericRepository<Brand,Integer> {

    @Override
    public Brand findById(Integer id) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        Brand brand=session.get(Brand.class,id);

        session.close();
        sessionFactory.close();

        return brand;
    }

    @Override
    public void add(Brand entity) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        session.save(entity);

        tx.commit();
        session.close();
        sessionFactory.close();
    }

    @Override
    public void update(Integer id,Brand entity) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        Brand brand=session.get(Brand.class,id);
        brand.setName(entity.getName());

        session.save(brand);

        tx.commit();
        session.close();
        sessionFactory.close();
    }

    @Override
    public void delete(Integer id) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        Brand brand=session.get(Brand.class,id.intValue());
        session.delete(brand);

        tx.commit();
        session.close();
        sessionFactory.close();;
    }

    @Override
    public List<Brand> findAll() {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        List<Brand> brands=(List<Brand>)session.createCriteria(Brand.class).list();

        session.close();
        sessionFactory.close();

        return brands;
    }

    public Brand findByName(String name) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        Query query=session.createQuery("FROM Brand B WHERE B.name= :name");
        query.setParameter("name",name);
        List<Brand> list=query.list();

        if(list==null || list.size()==0) return null;

        return list.get(0);
    }
}
