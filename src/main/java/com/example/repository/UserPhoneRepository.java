package com.example.repository;

import com.example.entities.User;
import com.example.entities.UserPhone;
import com.example.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("phoneRepository")
public class UserPhoneRepository implements GenericRepository<UserPhone,String>{

    @Override
    public UserPhone findById(String id) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        UserPhone phone=session.get(UserPhone.class,id);

        session.close();
        sessionFactory.close();

        return phone;
    }

    @Override
    public void add(UserPhone entity) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        session.save(entity);

        tx.commit();
        session.close();
        sessionFactory.close();
    }

    @Override
    public void update(String id,UserPhone entity) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        UserPhone phone=session.get(UserPhone.class,id);
        User user=entity.getUser();

        phone.setNum(entity.getNum());
        phone.setType(entity.getType());
        phone.setUser(entity.getUser());

        session.save(phone);
        user.getUserInfo().getPhones().add(phone);

        tx.commit();
        session.close();
        sessionFactory.close();
    }

    @Override
    public void delete(String id) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        UserPhone phone=session.get(UserPhone.class,id);
        session.delete(phone);

        tx.commit();
        session.close();
        sessionFactory.close();;
    }

    @Override
    public List<UserPhone> findAll() {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        List<UserPhone> phones=(List<UserPhone>)session.createCriteria(UserPhone.class).list();

        session.close();
        sessionFactory.close();

        return phones;
    }

    public UserPhone findByNumber(String number){
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        Query query=session.createQuery("FROM UserPhone U WHERE U.num= :number");
        query.setParameter("number",number);
        List<UserPhone> list=query.list();

        if(list==null || list.size()==0) return null;

        return list.get(0);
    }
}
