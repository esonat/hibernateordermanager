package com.example.repository;

import com.example.entities.Product;
import com.example.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("productRepository")
public class ProductRepository implements GenericRepository<Product,Integer>{

    @Override
    public Product findById(Integer id) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        Product product=session.get(Product.class,id);

        session.close();
        sessionFactory.close();

        return product;
    }

    @Override
    public void add(Product entity) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        session.save(entity);

        tx.commit();
        session.close();
        sessionFactory.close();
    }

    @Override
    public void update(Integer id,Product entity) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        Product product=session.get(Product.class,id);
        product.setName(entity.getName());
        product.setBrand(entity.getBrand());
        session.save(product);
        tx.commit();
        session.close();
        sessionFactory.close();
    }

    @Override
    public void delete(Integer id) {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        Product product=session.get(Product.class,id);
        session.delete(product);

        tx.commit();
        session.close();
        sessionFactory.close();;
    }

    @Override
    public List<Product> findAll() {
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();

        List<Product> products=(List<Product>)session.createCriteria(Product.class).list();

        session.close();
        sessionFactory.close();

        return products;
    }
}
