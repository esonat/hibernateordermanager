package com.example.exception;

public class DuplicateUsernameException extends Exception{

    public DuplicateUsernameException(String message) {
        super(message);
    }

    public DuplicateUsernameException(Throwable cause) {
        super(cause);
    }

    public DuplicateUsernameException(String message, Throwable cause) {
        super(message, cause);
    }
}
