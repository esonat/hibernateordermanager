package com.example.exception;

public class PhoneNotFoundException extends Exception {
    public PhoneNotFoundException(String message) {
        super(message);
    }

    public PhoneNotFoundException(Throwable cause) {
        super(cause);
    }

    public PhoneNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
