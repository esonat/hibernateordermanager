package com.example.exception;

public class DuplicatePhoneException extends Exception {

    public DuplicatePhoneException(String message) {
        super(message);
    }

    public DuplicatePhoneException(Throwable cause) {
        super(cause);
    }

    public DuplicatePhoneException(String message, Throwable cause) {
        super(message, cause);
    }
}
