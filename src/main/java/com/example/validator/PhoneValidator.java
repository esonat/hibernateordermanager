package com.example.validator;

import com.example.entities.Phone;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class PhoneValidator implements Validator{
    @Override
    public boolean supports(Class<?> clazz) {
        return Phone.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors,"num","error.num","Number is required");
        ValidationUtils.rejectIfEmpty(errors,"type","error.type","Type is required");
    }
}
