package com.example.validator;

import com.example.viewmodel.UserInsertViewModel;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return UserInsertViewModel.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors,"name",    "emptyName","Name cannot be empty");
        ValidationUtils.rejectIfEmpty(errors,"username","emptyUsername","Username cannot be empty");
        ValidationUtils.rejectIfEmpty(errors,"password","emptyPassword","Password cannot be empty");
        ValidationUtils.rejectIfEmpty(errors,"roles",   "emptyRoles","Roles cannot be empty");
        ValidationUtils.rejectIfEmpty(errors,"city",    "emptyCity","City cannot be empty");
        ValidationUtils.rejectIfEmpty(errors,"street",  "emptyStreet","Street cannot be empty");
        ValidationUtils.rejectIfEmpty(errors,"state",   "emptyState","State cannot be empty");
        ValidationUtils.rejectIfEmpty(errors,"zip",     "emptyZip","Zip cannot be empty");
        ValidationUtils.rejectIfEmpty(errors,"phones",  "emptyPhones","Phones cannot be empty");

        UserInsertViewModel user=(UserInsertViewModel)target;


        if(user.getPassword().length()<3 ||
           user.getPassword().length()>5)
            errors.rejectValue("password","passwordLength","Password must be between 3 to 5 characters");

        for(String phone:user.getPhones()){
            /*
            123-456-7890
            (123) 456-7890
            123 456 7890
            123.456.7890
            +91 (123) 456-7890
            */

            //final Pattern pattern = Pattern.compile("^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}$\n");
//            if(!pattern.matcher(phone).matches()){
//                errors.rejectValue("phones","phone.number","Invalid phone number");
//            }
            //^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$

            String regex="^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}$";
            Pattern pattern=Pattern.compile(regex);
            Matcher matcher=pattern.matcher(phone);

            if(!matcher.matches()){
                errors.rejectValue("phones","phoneNumber","Invalid phone number");
            }
        }
    }
}
