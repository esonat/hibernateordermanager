package com.example.config;


import com.example.validator.PhoneValidator;
import com.example.validator.UserValidator;
import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
public class BeanConfiguration {
    @Bean(name="mapper")
    public DozerBeanMapper mapper(){
        DozerBeanMapper mapper=new DozerBeanMapper();
        List<String> list= Arrays.asList("dozer-bean-mappings.xml");

        mapper.setMappingFiles(list);
        return mapper;
    }

    @Bean("phoneValidator")
    public PhoneValidator phoneValidator(){
        return new PhoneValidator();
    }

    @Bean("userValidator")
    public UserValidator userValidator(){
        return new UserValidator();
    }
}
