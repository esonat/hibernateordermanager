package com.example.entities;

public enum ShipmentStatus {
    NOTDELIVERED,
    ONDELIVERY,
    DELIEVERED
};