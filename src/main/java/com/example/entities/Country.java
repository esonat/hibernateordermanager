package com.example.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity(name="Country")
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Pattern(regexp ="[a-zA-Z]",message = "Country name must not contain numbers")
    @Size(min=2,max = 30,message = "Country name must be between 2 to 30 characters")
    @Column(name="country_name")
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "country",fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    @OrderBy("name ASC")
    private List<City> cities;

    public Country(){
        cities=new ArrayList<City>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }
}
