package com.example.entities;

import javax.persistence.Embeddable;
import javax.persistence.OneToOne;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@Embeddable
public class Address {
    @OneToOne
    private City city;

    private String street;

    private String state;

    @Pattern(regexp ="[0-9]",message = "Zip code must be only numbers")
    @Size(min=2,max = 30,message = "Zip code must be between 2 to 30 characters")
    private String zip;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
}
