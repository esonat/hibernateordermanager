package com.example.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Embeddable
public class UserInfo {

    @Column(name="name")
    private String name;

    @Embedded
    @AttributeOverrides(
        @AttributeOverride(
                name="zip",
                column = @Column(name="zip_code")
        )
    )
    private Address address;

    @OneToMany(mappedBy = "user",fetch = FetchType.EAGER,cascade = CascadeType.ALL,orphanRemoval = true)
    private Set<UserPhone> phones;


    public UserInfo(){
        phones=new HashSet<UserPhone>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<UserPhone> getPhones() {
        return phones;
    }

    public void setPhones(Set<UserPhone> phones) {
        this.phones = phones;
    }
}
