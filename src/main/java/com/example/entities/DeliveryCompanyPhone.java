package com.example.entities;

import javax.persistence.*;

@Entity
@Table(name="DeliveryCompanyPhone")
@DiscriminatorValue("deliverycompany")
public class DeliveryCompanyPhone extends Phone {

    @ManyToOne
    @JoinColumn(name="deliverycompany_id")
    private DeliveryCompany company;

    public DeliveryCompany getCompany() {
        return company;
    }

    public void setCompany(DeliveryCompany company) {
        this.company = company;
    }
}
