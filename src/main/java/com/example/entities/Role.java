package com.example.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Role {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonIgnore
    @Version
    private int version;

    @Pattern(regexp = "[a-zA-Z]+",message = "Role name must be only characters")
    @Column(name="role_name")
    private String name;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "role",cascade = CascadeType.PERSIST)
    private List<UserRole> userRoles=new ArrayList<UserRole>();

    public Role(){
        userRoles=new ArrayList<UserRole>();
    }

    public Role(String name){
        this.name=name;
        userRoles=new ArrayList<UserRole>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserRole> users) {
        this.userRoles= users;
    }
}
