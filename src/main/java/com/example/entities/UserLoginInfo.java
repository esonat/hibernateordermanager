package com.example.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import java.util.Date;

@Embeddable
public class UserLoginInfo {
    @JsonProperty
    @Column(name="username",unique = true)
    private String username;

    @JsonProperty
    @Column(name="password")
    @Size(min=3,max=5,message = "Password must be between 3 and 5 characters")
    private String password;

    @JsonProperty
    @Temporal(TemporalType.TIME)
    private Date lastLoginTime;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }
}
