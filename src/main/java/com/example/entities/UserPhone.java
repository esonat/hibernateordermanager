package com.example.entities;

import javax.persistence.*;

@Entity
@Table(name="UserPhone")
@DiscriminatorValue("user")
public class UserPhone extends Phone{

    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
