package com.example.entities;

public enum  DeliveryMethod {
    AIR,
    SEA,
    LAND
};
