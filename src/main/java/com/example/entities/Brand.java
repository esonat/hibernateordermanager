package com.example.entities;


import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity(name="Brand")
public class Brand{

    @Id
    @GeneratedValue
    private int id;

    @Size(min=2,max = 30,message = "Brand name must be between 2 to 30 characters")
    private String name;

    @OneToMany(mappedBy = "brand",cascade = CascadeType.ALL)
    private List<Product> products;

    public Brand(){}
    public Brand(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
