package com.example.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

public class UserRoleId implements Serializable {
    @JsonIgnore
    private int user;
    @JsonIgnore
    private int role;

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }
}
