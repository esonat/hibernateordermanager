package com.example.entities;

import javax.persistence.*;
import java.util.UUID;

@Entity(name="Shipment")
public class Shipment {
    @Id
    @GeneratedValue
    private int id;

    private UUID shipmentId;

    @Enumerated(EnumType.STRING)
    @Column(name="status")
    private ShipmentStatus status;

    @OneToOne
    @JoinColumn(name="order_id")
    private Order order;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UUID getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(UUID shipmentId) {
        this.shipmentId = shipmentId;
    }

    public ShipmentStatus getStatus() {
        return status;
    }

    public void setStatus(ShipmentStatus status) {
        this.status = status;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
