package com.example.entities;

import com.example.valuegeneration.FunctionCreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity(name="Order")
public class Order {

    @Id
    @GeneratedValue
    private int id;

    private UUID uuid;

    @Column(name="datetime")
    @FunctionCreationTimestamp
    private Date datetime;

    @OneToMany(mappedBy = "order")
    private List<OrderItem> items;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Date getTimestamp() {
        return datetime;
    }

    public void setTimestamp(Date datetime) {
        this.datetime=datetime;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }
}