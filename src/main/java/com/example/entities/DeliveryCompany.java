package com.example.entities;

import javax.persistence.*;
import java.util.List;

@Entity(name="DeliveryCompany")
public class DeliveryCompany {

    @Id
    @GeneratedValue
    private int id;

    private String name;

    @Embedded
    private Address address;

    @OneToMany(mappedBy = "company",fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    private List<DeliveryCompanyPhone> phones;

    @Enumerated(EnumType.STRING)
    @Column(name="method")
    private DeliveryMethod deliveryMethod;

}
