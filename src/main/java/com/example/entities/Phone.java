package com.example.entities;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

@Entity
@Table(name="Phone")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="field",discriminatorType = DiscriminatorType.STRING)
public class Phone{
    @Id
    String num;

    String type;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String toString() {
        return "Phone num: " + num + " type: " + type;
    }
}
