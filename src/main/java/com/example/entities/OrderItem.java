package com.example.entities;

import javax.persistence.*;
import javax.persistence.Entity;

@Entity(name="OrderItem")
public class OrderItem{

    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    @JoinColumn(name="product")
    private Product product;

    private int quantity;

    @ManyToOne
    @JoinColumn(name = "order")
    private Order order;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
