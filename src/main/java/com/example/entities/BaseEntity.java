package com.example.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public abstract class BaseEntity<T extends Serializable> {
    @Id
    @GeneratedValue
    T id;

    public BaseEntity(){}
    public BaseEntity(T id){
        this.id=id;
    }

    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }
}
