package com.example.converter;

import com.example.entities.Brand;
import org.dozer.DozerConverter;

public class BrandConverter extends DozerConverter<Brand,String>{
    public BrandConverter(){
        super(Brand.class,String.class);
    }

    public String convertTo(Brand source,String destination){
        if(source.getName().isEmpty() ||
                source.getName()==null)
            throw new IllegalStateException("Name must not be null");

        return source.getName();
    }

    public Brand convertFrom(String source,Brand destination){
        Brand brand=new Brand(source);

        return brand;
    }
}
