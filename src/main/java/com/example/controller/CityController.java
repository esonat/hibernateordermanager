package com.example.controller;

import com.example.entities.City;
import com.example.entities.Country;
import com.example.repository.CityRepository;
import com.example.repository.CountryRepository;
import com.example.viewmodel.CityViewModel;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private DozerBeanMapper mapper;


    @RequestMapping("/cities")
    public String findAllCities(Model model){
        List<CityViewModel> cities=new ArrayList<>();

        for(City city:cityRepository.findAll()){
            CityViewModel vm=new CityViewModel(
                    city.getId(),
                    city.getName(),
                    city.getCountry().getName());

            cities.add(vm);
        }

        model.addAttribute("cities",cities);
        return "cities";
    }

    @RequestMapping(value="/addCity",method = RequestMethod.GET)
    public String addCityForm(@ModelAttribute("city")CityViewModel cityVM,
                                 Model model){
        List<String> countries=new ArrayList<>();

        for(Country country:countryRepository.findAll()){
            countries.add(country.getName());
        }

        model.addAttribute("countries",countries);

        return "addCity";
    }

    @RequestMapping(value = "/addCity",method = RequestMethod.POST)
    public String addCity(@ModelAttribute("city") CityViewModel cityVM,
                             Model model){
        City city=new City();
        city.setName(cityVM.getName());

        Country country=countryRepository.findByName(cityVM.getCountry());
        city.setCountry(country);

        cityRepository.add(city);

        return "redirect:/cities";
    }

    @RequestMapping(value="/cities/{id}/delete",method = RequestMethod.POST)
    public String deleteBrand(@PathVariable("id")int id){
        cityRepository.delete(id);
        return "redirect:/cities";
    }

}
