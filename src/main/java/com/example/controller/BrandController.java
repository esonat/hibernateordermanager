package com.example.controller;

import com.example.entities.Brand;
import com.example.repository.BrandRepository;
import com.example.viewmodel.BrandViewModel;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
public class BrandController {


    @Autowired
    private BrandRepository brandRepository;

    @Autowired
    private DozerBeanMapper mapper;


    @RequestMapping("/brands")
    public String findAllBrands(Model model){
        List<BrandViewModel> list=new ArrayList<>();

        for(Brand brand:brandRepository.findAll()){
            BrandViewModel vm=new BrandViewModel(brand.getId(),brand.getName());
            list.add(vm);
        }
        model.addAttribute("brands",list);
        return "brands";
    }

    @RequestMapping(value="/addBrand",method = RequestMethod.GET)
    public String addBrandForm(@ModelAttribute("brand")BrandViewModel brandVM,
                                 Model model){
        return "addBrand";
    }

    @RequestMapping(value = "/addBrand",method = RequestMethod.POST)
    public String addBrand(@ModelAttribute("brand") BrandViewModel brandVM,
                             Model model){
        Brand brand=new Brand(brandVM.getName());
        brandRepository.add(brand);

        return "redirect:/brands";
    }

    @RequestMapping(value="/brands/{id}/delete",method = RequestMethod.POST)
    public String deleteBrand(@PathVariable("id")int id){
        brandRepository.delete(id);

        return "redirect:/brands";
    }

}
