package com.example.controller;

import com.example.entities.Country;
import com.example.repository.CountryRepository;
import com.example.viewmodel.CountryViewModel;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
public class CountryController {

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private DozerBeanMapper mapper;


    @RequestMapping("/countries")
    public String findAllCountries(Model model) throws Exception{
        List<CountryViewModel> countries=new ArrayList<>();

       for(Country country:countryRepository.findAll()) {
           countries.add(new CountryViewModel(
                   country.getId(),
                   country.getName()));
       }

        model.addAttribute("countries",countries);
        return "countries";
    }

    @RequestMapping(value="/addCountry",method = RequestMethod.GET)
    public String addCountryForm(Model model){

        return "addCountry";
    }

    @RequestMapping(value = "/addCountry",method = RequestMethod.POST)
    public String addCountry(String name,Model model) throws Exception{

        Country country=new Country();
        country.setName(name);

        countryRepository.add(country);

        return "redirect:/countries";
    }


    @RequestMapping(value="/countries/{id}/delete",method = RequestMethod.POST)
    public String deleteBrand(@PathVariable("id")int id) throws Exception{
        countryRepository.delete(id);
        return "redirect:/countries";
    }
}
