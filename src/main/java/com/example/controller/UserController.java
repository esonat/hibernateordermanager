package com.example.controller;


import com.example.entities.*;
import com.example.exception.DuplicatePhoneException;
import com.example.exception.DuplicateUsernameException;
import com.example.exception.RoleNotFoundException;
import com.example.repository.CityRepository;
import com.example.repository.RoleRepository;
import com.example.repository.UserRepository;
import com.example.service.MainService;
import com.example.validator.UserValidator;
import com.example.viewmodel.UserInsertViewModel;
import com.example.viewmodel.UserViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private MainService mainService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private UserValidator userValidator;

    @RequestMapping("/users")
    public String findAllUsers(Model model) throws Exception{
        List<UserViewModel> list=new ArrayList<>();

        for(User user:userRepository.findAll()){

            List<String> roles=new ArrayList<>();
            List<String> phones=new ArrayList<>();

            for(UserRole userRole:user.getUserRoles()){
                roles.add(userRole.getRole().getName());
            }
            for(UserPhone phone:user.getUserInfo().getPhones()){
                phones.add(phone.getNum());
            }

            UserViewModel vm=new UserViewModel(
                    user.getId(),
                    user.getUserInfo().getName(),
                    user.getUserLoginInfo().getUsername(),
                    user.getUserLoginInfo().getPassword(),
                    roles,
                    null,
                    user.getUserInfo().getAddress().getCity().getName(),
                    user.getUserInfo().getAddress().getStreet(),
                    user.getUserInfo().getAddress().getState(),
                    user.getUserInfo().getAddress().getZip(),
                    phones);

            list.add(vm);
        }
        model.addAttribute("users",list);
        return "users";
    }


    @RequestMapping(value="/addUser",method = RequestMethod.GET)
    public ModelAndView addUserForm(@ModelAttribute("user")UserInsertViewModel userVM,
                                    Model model){

        List<String> roles=new ArrayList<>();
        List<String> cities=new ArrayList<>();

        for(Role role:roleRepository.findAll()) roles.add(role.getName());
        for(City city:cityRepository.findAll()) cities.add(city.getName());

        model.addAttribute("roles",roles);
        model.addAttribute("cities",cities);

        ModelAndView mav=new ModelAndView();

        mav.setViewName("addUser");
        mav.addObject("roles",roles);
        mav.addObject("cities",cities);

        return mav;
    }

    public List<String> roles(){
        List<String> roles=new ArrayList<>();
        for(Role role:roleRepository.findAll()) roles.add(role.getName());

        return roles;
    }
    public List<String> cities(){
        List<String> cities=new ArrayList<>();
        for(City city:cityRepository.findAll()) cities.add(city.getName());

        return cities;
    }

    @RequestMapping(value = "/addUser",method = RequestMethod.POST)
    public String addUser(@ModelAttribute("user")UserInsertViewModel userVM,
                             BindingResult result,
                             Model model) throws Exception{
        try {

            userValidator.validate(userVM,result);

            if(result.hasErrors()){
                for(ObjectError error:result.getAllErrors()){
                    model.addAttribute(error.getCode(),error.getDefaultMessage());
                }

                model.addAttribute("roles",roles());
                model.addAttribute("cities",cities());

                return "addUser";
            }

            mainService.addUser(userVM);

        }catch(RoleNotFoundException e){
            model.addAttribute("error","Role not found");
            model.addAttribute("roles",roles());
            model.addAttribute("cities",cities());

            return "addUser";
        }catch (DuplicatePhoneException e){
            model.addAttribute("error","Duplicate phone");
            model.addAttribute("roles",roles());
            model.addAttribute("cities",cities());

            return "addUser";
        }catch (DuplicateUsernameException e){
            model.addAttribute("error","Duplicate username");
            model.addAttribute("roles",roles());
            model.addAttribute("cities",cities());

            return "addUser";
        }

        return "redirect:/users";
    }

    @RequestMapping(value="/users/{id}/delete",method = RequestMethod.POST)
    public String deleteUser(@PathVariable("id")int id) throws Exception{
        userRepository.delete(id);
        return "redirect:/users";
    }
}
