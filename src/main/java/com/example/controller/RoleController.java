package com.example.controller;

import com.example.entities.Role;
import com.example.repository.RoleRepository;
import com.example.viewmodel.RoleViewModel;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
public class RoleController {
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private DozerBeanMapper mapper;


    @RequestMapping("/roles")
    public String findAllRoles(Model model) throws Exception{
        List<RoleViewModel> list=new ArrayList<>();

        for(Role role:roleRepository.findAll()){
            list.add(new RoleViewModel(
                    role.getId(),
                    role.getName()
            ));
        }

        model.addAttribute("roles",list);
        return "roles";
    }

    @RequestMapping(value="/addRole",method = RequestMethod.GET)
    public String addRoleForm(String name,Model model) throws Exception{
        return "addRole";
    }

    @RequestMapping(value = "/addRole",method = RequestMethod.POST)
    public String addRole(String name,Model model) throws Exception{
        Role role=new Role();
        role.setName(name);

        roleRepository.add(role);

        return "redirect:/roles";
    }


    @RequestMapping(value="/roles/{id}/delete",method = RequestMethod.POST)
    public String deleteBrand(@PathVariable("id")int id) throws Exception{
        roleRepository.delete(id);
        return "redirect:/roles";
    }
}