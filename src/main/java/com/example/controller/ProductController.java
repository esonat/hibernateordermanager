package com.example.controller;


import com.example.entities.Brand;
import com.example.entities.Product;
import com.example.repository.BrandRepository;
import com.example.repository.ProductRepository;
import com.example.viewmodel.ProductViewModel;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ProductController {
    private Logger logger= LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private BrandRepository brandRepository;

    @Autowired
    private DozerBeanMapper mapper;

    @RequestMapping("/products")
    public String findAllProducts(Model model){
        List<ProductViewModel> list=new ArrayList<>();

        for(Product product:productRepository.findAll()){
            ProductViewModel vm=new ProductViewModel();
            mapper.map(product,vm);
            list.add(vm);
        }

        model.addAttribute("products",list);
        return "products";
    }

    @RequestMapping(value="/addProduct",method = RequestMethod.GET)
    public String addProductForm(@ModelAttribute("product")ProductViewModel productVM,
                                 Model model){
        model.addAttribute("brands",brandRepository.findAll());
        return "addProduct";
    }

    @RequestMapping(value = "/addProduct",method = RequestMethod.POST)
    public String addProduct(@ModelAttribute("product") ProductViewModel productVM,
                             Model model){
        Product product=new Product();
        Brand brand=brandRepository.findByName(productVM.getBrandName());

        product.setName(productVM.getName());
        product.setBrand(brand);

        productRepository.add(product);

        return "redirect:/products";
    }

    @RequestMapping(value = "/products/{id}/delete",method = RequestMethod.POST)
    public String deleteProduct(@PathVariable("id")int id){

        productRepository.delete(id);
        return "redirect:/products";
    }

}
