package com.example.viewmodel;

import java.util.List;

public class UserViewModel {
    private int id;
    private String name;
    private String username;
    private String password;
    private List<String> roles;
    private String lastLoginTime;
    private String city;
    private String street;
    private String state;
    private String zip;
    private List<String> phones;

    public UserViewModel(){}

    public UserViewModel(int id,String name, String username, String password, List<String> roles,String lastLoginTime, String city, String street, String state, String zip, List<String> phones) {
        this.id=id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.roles=roles;
        this.lastLoginTime = lastLoginTime;
        this.city = city;
        this.street = street;
        this.state = state;
        this.zip = zip;
        this.phones = phones;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public List<String> getPhones() {
        return phones;
    }

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }
}
