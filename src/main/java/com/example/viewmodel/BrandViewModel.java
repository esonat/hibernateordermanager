package com.example.viewmodel;

public class BrandViewModel {
    private int id;
    private String name;

    public BrandViewModel(){}
    public BrandViewModel(int id,String name){
        this.name=name;
        this.id=id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
