package com.example.viewmodel;

import java.util.List;

public class UserInsertViewModel {
    private String name;
    private String username;
    private String password;
    private List<String> roles;
    private String city;
    private String street;
    private String state;
    private String zip;
    private List<String> phones;

    public UserInsertViewModel(){}

    public UserInsertViewModel(String name, String username, String password, List<String> roles,String lastLoginTime, String city, String street, String state, String zip, List<String> phones) {

        this.name = name;
        this.username = username;
        this.password = password;
        this.roles=roles;
        this.city = city;
        this.street = street;
        this.state = state;
        this.zip = zip;
        this.phones = phones;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public List<String> getPhones() {
        return phones;
    }

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }
}
