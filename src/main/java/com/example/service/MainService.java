package com.example.service;

import com.example.entities.*;
import com.example.exception.DuplicatePhoneException;
import com.example.exception.DuplicateUsernameException;
import com.example.exception.RoleNotFoundException;
import com.example.repository.CityRepository;
import com.example.repository.RoleRepository;
import com.example.repository.UserPhoneRepository;
import com.example.repository.UserRepository;
import com.example.viewmodel.UserInsertViewModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MainService {

    private Logger logger= LoggerFactory.getLogger(MainService.class);

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserPhoneRepository phoneRepository;

    @Autowired
    private CityRepository cityRepository;

    public void addUser(UserInsertViewModel userVM) throws Exception{
        List<String> numbers=userVM.getPhones();
        List<String> roleNames=userVM.getRoles();

        List<UserPhone> phones=new ArrayList<>();
        List<Role> roles=new ArrayList<>();

        for(User user:userRepository.findAll()){
            if(user.getUserLoginInfo().getUsername().equals(userVM.getUsername())){
                logger.error("Duplicate username:"+userVM.getUsername());
                throw new DuplicateUsernameException("Duplicate username");
            }
        }

        for(String number:numbers){
            if(phoneRepository.findByNumber(number)!=null){
                logger.error("Duplicate phone");
                throw new DuplicatePhoneException("Duplicate phone");
            }

            UserPhone phone=new UserPhone();
            phone.setNum(number);
            phone.setType("user");

            phones.add(phone);
        }
        for(String roleName:roleNames){
            if(roleRepository.findByName(roleName)==null) {
                logger.error("Role not found");
                throw new RoleNotFoundException("Role not found");
            }
            Role role=roleRepository.findByName(roleName);
            roles.add(role);
        }

        User user=new User();
        UserInfo userInfo=new UserInfo();
        UserLoginInfo userLoginInfo=new UserLoginInfo();

        City city=cityRepository.findByName(userVM.getCity());

        Address address=new Address();
        address.setState(userVM.getState());
        address.setZip(userVM.getZip());
        address.setStreet(userVM.getStreet());
        address.setCity(city);

        userInfo.setAddress(address);
        userInfo.setName(userVM.getName());

        userLoginInfo.setUsername(userVM.getUsername());
        userLoginInfo.setPassword(userVM.getPassword());
        userLoginInfo.setLastLoginTime(null);

        user.setUserInfo(userInfo);
        user.setUserLoginInfo(userLoginInfo);

        userRepository.add(user,phones,roles);
    }
}
