package com.example.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

    public static SessionFactory getSessionFactory(){
        SessionFactory sessionFactory;
        sessionFactory=new Configuration()
                .configure()
                .buildSessionFactory();

        //Session session=sessionFactory.openSession();
        return sessionFactory;
    }
}
